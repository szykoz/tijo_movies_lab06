package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieLongMapper implements Converter <List<Movie>, MovieCounterDto> {

    @Override
    public MovieCounterDto convert (List<Movie> movies) {

        MovieCounterDto movieCounterDto = new MovieCounterDto();

        for(Movie movie:movies) {
            if(movie.getMovieId()!= null) {
                movieCounterDto.setCounter(movieCounterDto.getCounter()+1L);
            }
        }
        return movieCounterDto;
    }
}

